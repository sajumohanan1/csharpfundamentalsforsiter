﻿//container for the classes
namespace CSharpFundamentals
{
    //A class is a blueprint for the methods, properties, variables
    internal class Program
    {
        //entry point of your application
        //run your program at main
        static void Main(string[] args)
        {
            /* Variables
             * Is a temporary storage unit - to store the values
             * variable can hold only single value
             * temporary storage
             * categories
             *      (1) Primitives (value types)
             *      (2) Objects (Reference types)
             * Primitives
             *  (1) int, char, stirng, bool, datetime             *    
             */


            //Naming convention - meaningful, camelCase, singleWord
            //Creating variable and initialize value
            int itemQuantity = 10;
            //print value from variable using CW
            Console.WriteLine("Item Quantity: " + itemQuantity);
            //Convert operator to string interpolation
            Console.WriteLine($"Item Quantity:  { itemQuantity}");
            //to add some text/message --> string
            //using + operator, string concatenation

            //c sharp is case sensitive

            char itemCode = 'A';
            string itemName = "AAA";
            double itemPrice = 234.23;
            float itemAmount = 233.22F;//consider as double

            bool itemAvailability = true;
            bool? isItemAvailable = false;

            Console.WriteLine(isItemAvailable);

            //Type conversion
            string numberAsWord = "34343";
            int number=int.Parse(numberAsWord);//convert as number
            Console.WriteLine(number);//print number

            string numberAsWordAgain = number.ToString();//now it is a string
            Console.WriteLine(numberAsWordAgain);


            /* Collections 
             * for grouping data - collection can hold multiple values
             * Array
             *  (1) One dimensional array
             *  (2) Two dimensional array
             *  (3) Jagged array
             * Generic Collections
             *  (1) Dictonary
             *  (2) List
             *  (3) SortedList
             *  (4) Queue
             *  (5) Stack
             */

            //Create an array
            //Array is also a variable
            //This is a 1 dimensional array
            int[] itemQty = new int[4]; //n-1=3
            itemQty[0] = 10;
            itemQty[1] = 11;
            itemQty[2] = 12;
            itemQty[3] = 13;
            Console.WriteLine(itemQty[3]);//print the array value from index 0
            Console.WriteLine(itemQty[2]);//print array elment from address 2
            int total = itemQty[0] + itemQty[2];
            Console.WriteLine(total);

            //Two deminsional array - form of a table (rows x columns)
            int[,] itemModel = new int[4,5];
            itemModel[2,1] = 25;
            itemModel[1,0] = 20;
            Console.WriteLine(itemModel[3, 1]);

            //use Exception handling

            //Generic collections
            //Array is not flexible, fixed
            //Generic is flexible and easy to add, delete items

            //Dictionary is created with Key-Integer and Value-String
            Dictionary<string, string> itemDictionary = new Dictionary<string, string>();
            itemDictionary.Add("AB","Television");           
            itemDictionary.Add("CD","Refrigerator");           
            itemDictionary.Add("ED","Smart Phone");

            //Loop - Iteraction
           //loop will run as long as the condition is true#
         //loop will stop execution when the condition become false
            foreach (var item in itemDictionary)
            {
                //action - statement will be running
                Console.WriteLine(item.Value);
            }

            //If statement
            if (itemDictionary.ContainsKey("ABCD"))//expression is true
            {
                //add some statements (perform when expression is true)
                Console.WriteLine(itemDictionary["AB"]);
            }
            else//to run the else part when the condition become false
            {
                Console.WriteLine("Invalid Key");//perform when expression is false
                //add some other statements
            }

            //List
            List<string> items = new List<string>();
            items.Add("Apple");
            items.Add("Orange");
            Console.WriteLine(items[0]);

            Console.ReadKey();
        }
        
    }
    
}